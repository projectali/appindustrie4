import './styles/app.scss';
import 'bootstrap/dist/js/bootstrap.bundle.js';
import 'datatables.net';
import 'datatables.net-bs5';

import './bootstrap.js';
// import 'bootstrap';
$(document).ready(function() {
  $('#datatable').DataTable({
    'language': {
      'lengthMenu': 'Afficher _MENU_ enregistrements par page',
      'zeroRecords': 'Aucun enregistrement trouvez - oops',
      'info': 'Montrer page _PAGE_ de _PAGES_',
      'infoEmpty': 'Pas d\'enregistrement touver',
      'infoFiltered': '(filtrer à apartir de _MAX_ totales enregistrements)',
      'search': 'rechercher',
      'paginate': {
        'first': 'Premier',
        'last': 'Dernier',
        'next': 'Suivant',
        'previous': 'Précédent'
      },
    },

  });
});

// register globally for all charts
document.addEventListener('chartjs:init', function(event) {
  const Chart = event.detail.Chart;
  const Tooltip = Chart.registry.plugins.get('tooltip');
  Tooltip.positioners.bottom = function(items) {};
});

import './admin2/bootstrap/js/bootstrap.bundle.min.js';
import './admin2/jquery-easing/jquery.easing.min.js';
import './admin2/js/sb-admin-2.min.js';


$('#btnOneclic').on('click', function() {
  $('#start').val();
  $('#datedebuttosubmit').val();
  $('#datedebuttosubmit').val($('#start').val());
});

$('#btnTwoclic').on('click', function() {
  $('#start2').val();
  $('#end2').val();
  $('#datedebuttosubmit').val();
  $('#datefintosubmit').val();

  $('#datedebuttosubmit').val($('#start2').val());
  $('#datefintosubmit').val($('#end2').val());
});
$('.alert').delay(4000).slideUp(200, function() {
  $(this).alert('close');
});


// $(document).ready(function() {
//   // $('#button_building').click(function() {
//   alert('Test');
//   // });
// });
