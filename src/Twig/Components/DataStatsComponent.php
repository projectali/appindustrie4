<?php

namespace App\Twig\Components;

use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\DefaultActionTrait;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use App\Repository\TableJournaleRepository;
use Doctrine\Common\Collections\Expr\Value;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;


        function date_range($first, $last, $step = '+1 day', $output_format = 'd/m/Y' ) 
        {
        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);


        while( $current <= $last ) {

                $dates[] = date($output_format, $current);
                $current = strtotime($step, $current);
        }

        return $dates;
        }

        function labelArray(string $label, int $labelStep, int $labelCount): array
        {
                $labelOut  = array();
                if (($label == 's') and ($labelStep == 1)) {    //0
                 for ($i=0; $i < $labelCount -1; $i++) { 
                        $labelOut[$i] = (string)($i * $labelStep) .$label; 
                        }                      
                }elseif (($label == 'm') and ($labelStep == 1)) {       //1
                  for ($i=0; $i < $labelCount -1; $i++) { 
                        $labelOut[$i] = (string)($i * $labelStep) .$label;
                        }
                }elseif (($label == 'm') and ($labelStep == 30)) {      //2
                  for ($i=0; $i < $labelCount -1; $i++) { 
                        $labelOut[$i] = (string)($i * $labelStep) .$label;
                        }
                }elseif (($label == 'h') and ($labelStep == 1)) {       //3
                  for ($i=0; $i < $labelCount -1; $i++) { 
                        $labelOut[$i] = (string)($i * $labelStep) .$label;
                        }
                }elseif (($label == 'h') and ($labelStep == 4)) {       //4
                  for ($i=0; $i < $labelCount -1; $i++) { 
                        $labelOut[$i] = (string)($i * $labelStep) .$label;
                        }
                }elseif (($label == 'j') and ($labelStep == 1)) {       //5
                  for ($i=0; $i < $labelCount -1; $i++) { 
                        $labelOut[$i] = (string)($i * $labelStep) .$label;
                        }
                }elseif (($label == 'sem') and ($labelStep == 1)) {     //6
                  for ($i=0; $i < $labelCount -1; $i++) { 
                        $labelOut[$i] = (string)($i * $labelStep) .$label;
                        }
                } 

                return $labelOut;
        }

#[AsLiveComponent('DataStatsComponent')]
final class DataStatsComponent
{
    use DefaultActionTrait;

    public function __construct(
        private TableJournaleRepository $repo,
        private ChartBuilderInterface $chartBuilder
        )
    {
        
    } 

    #[LiveProp(writable: true)]
    public int $datavalue = 400;  

    #[LiveProp(writable: true)]
    public int $selectdate = 0; 

    #[LiveProp(writable: true)]
    public string $datedebut;

    #[LiveProp(writable: true)]
    public string $datefin;

    #[LiveProp(writable: true)]
    public string $machine;

    public function shartdesigne()
        {
                $chart = $this->chartBuilder->createChart(Chart::TYPE_BAR);
                // $chartlabels =  labelArray('s', 15, 21);
                $chartlabels = ['6H', '7H', '8H', '9H', '10H', '11H', '12H', '13H', '14H', '15H', '16H', '17H', '18H'];
                dump($chartlabels);
                $now = new \DateTime();
                if ($this->selectdate == '0') {
                        $this->datedebut = $now->format("Y-m-d");
                        $datas = $this->repo->getmachinepardate($this->datedebut, $this->machine);
                        // dump('selectdate in fetch data = 0');
                }elseif ($this->selectdate == '1') {
                        $now = $now->sub(new \DateInterval('P1D'));
                        $datedebut = $now->format("Y-m-d");
                        $datas = $this->repo->getmachinepardate($datedebut, $this->machine);
                        // dump('selectdate in fetch data = 1');
                }elseif ($this->selectdate == '2') {
                        $now = $now->sub(new \DateInterval('P2D'));
                        $this->datedebut = $now->format("Y-m-d");
                        $datas = $this->repo->getmachinepardate($this->datedebut, $this->machine);
                        // dump('selectdate in fetch data = 2');
                }elseif ($this->selectdate == '3') {
                        $datas = $this->repo->getmachinepardate($this->datedebut, $this->machine);
                        // dump('selectdate in fetch data = 3');
                }elseif ($this->selectdate == '4') {
                        $datas = $this->repo->getReport($this->datedebut, $this->datefin, $this->machine);
                        // dump('selectdate in fetch data = 4');
                        // dump($datas);
                }else {
                        $datas = $this->repo->findAll();
                }

                dump($this->datavalue);
                if ($this->datavalue <= 400 and $this->datavalue > 350) {       // 7    par semaine ou par mois 
                        $resolution = 9;
                        if ($this->selectdate == '0') {
                                $chartlabels = ['6H', '7H', '8H', '9H', '10H', '11H', '12H', '13H', '14H', '15H', '16H', '17H', '18H'];
                        }elseif ($this->selectdate == '1') {
                                $chartlabels = ['6H', '7H', '8H', '9H', '10H', '11H', '12H', '13H', '14H', '15H', '16H', '17H', '18H'];
                        }elseif ($this->selectdate == '2') {
                                $chartlabels = ['6H', '7H', '8H', '9H', '10H', '11H', '12H', '13H', '14H', '15H', '16H', '17H', '18H'];
                        }elseif ($this->selectdate == '3') {
                                $chartlabels = ['6H', '7H', '8H', '9H', '10H', '11H', '12H', '13H', '14H', '15H', '16H', '17H', '18H'];
                        }elseif ($this->selectdate == '4') {
                                $chartlabels = ['6H', '7H', '8H', '9H', '10H', '11H', '12H', '13H', '14H', '15H', '16H', '17H', '18H'];
                        }else {
                                $chartlabels = ['6H', '7H', '8H', '9H', '10H', '11H', '12H', '13H', '14H', '15H', '16H', '17H', '18H'];
                        }

                        if (count($datas) > 0) {
                                $chartdatas = [];
                                // nombre de points par heures
                                foreach ($datas as $key => $variable) {
                                        $chartdatas[count($chartdatas)] = $datas[$key]->getPointnumber();
                                }

                                $chart = $this->chartBuilder->createChart(Chart::TYPE_BAR);
                                $chart->setData(['labels' => $chartlabels,'datasets' => [['label' => $this->machine,'backgroundColor' => 'rgb(255, 99, 132, .4)','borderColor' => 'rgb(255, 99, 132)','data' => $chartdatas,'tension' => 0.4,],],]);
                                $chart->setOptions(['plugins' => ['title' => ['display' => true,'text' => sprintf(''),],'legend' => ['labels' => ['boxHeight' => 20,'boxWidth' => 50,'padding' => 20,'font' => ['size' => 14,],],],],'elements' => ['line' => ['borderWidth' => 5,'tension' => 0.25,'borderCapStyle' => 'round','borderJoinStyle' => 'round',],],'maintainAspectRatio' => false,]);            
                        }                        

                }elseif ($this->datavalue <= 350 and $this->datavalue > 300) {  // 6    1 journée
                        $resolution = 8;
                // }else {
                        $chartlabels = ['6H', '7H', '8H', '9H', '10H', '11H', '12H', '13H', '14H', '15H', '16H', '17H', '18H'];
                        $chart = $this->chartBuilder->createChart(Chart::TYPE_BAR);
                        $chart->setData(['labels' => $chartlabels,'datasets' => [['label' => $this->machine,'backgroundColor' => 'rgb(255, 99, 132, .4)','borderColor' => 'rgb(255, 99, 132)','data' => [0,0,0,0,0,0,0,0,0,0 ],'tension' => 0.4,],],]);
                        $chart->setOptions(['maintainAspectRatio' => false,]);
                }elseif ($this->datavalue <= 300 and $this->datavalue > 250) {  // 5    demie journée
                        $chartlabels = ['6H', '7H', '8H', '9H', '10H', '11H', '12H', '13H', '14H', '15H', '16H', '17H', '18H'];
                        $chart = $this->chartBuilder->createChart(Chart::TYPE_BAR);
                        $chart->setData(['labels' => $chartlabels,'datasets' => [['label' => $this->machine,'backgroundColor' => 'rgb(255, 99, 132, .4)','borderColor' => 'rgb(255, 99, 132)','data' => [0,0,0,0,0,0,0,0,0,0 ],'tension' => 0.4,],],]);
                        $chart->setOptions(['maintainAspectRatio' => false,]);
                }elseif ($this->datavalue <= 250 and $this->datavalue > 200) {  // 4    1 heure
                        $chartlabels = ['6H', '7H', '8H', '9H', '10H', '11H', '12H', '13H', '14H', '15H', '16H', '17H', '18H'];
                        $chart = $this->chartBuilder->createChart(Chart::TYPE_BAR);
                        $chart->setData(['labels' => $chartlabels,'datasets' => [['label' => $this->machine,'backgroundColor' => 'rgb(255, 99, 132, .4)','borderColor' => 'rgb(255, 99, 132)','data' => [0,0,0,0,0,0,0,0,0,0 ],'tension' => 0.4,],],]);
                        $chart->setOptions(['maintainAspectRatio' => false,]);
                }elseif ($this->datavalue <= 200 and $this->datavalue > 150) {  // 3    30 minute
                        $chartlabels = ['6H', '7H', '8H', '9H', '10H', '11H', '12H', '13H', '14H', '15H', '16H', '17H', '18H'];
                        $chart = $this->chartBuilder->createChart(Chart::TYPE_BAR);
                        $chart->setData(['labels' => $chartlabels,'datasets' => [['label' => $this->machine,'backgroundColor' => 'rgb(255, 99, 132, .4)','borderColor' => 'rgb(255, 99, 132)','data' => [0,0,0,0,0,0,0,0,0,0 ],'tension' => 0.4,],],]);
                        $chart->setOptions(['maintainAspectRatio' => false,]);
                }elseif ($this->datavalue <= 150 and $this->datavalue > 100) {  // 2    15 minute
                        $chartlabels = ['6H', '7H', '8H', '9H', '10H', '11H', '12H', '13H', '14H', '15H', '16H', '17H', '18H'];
                        $chart = $this->chartBuilder->createChart(Chart::TYPE_BAR);
                        $chart->setData(['labels' => $chartlabels,'datasets' => [['label' => $this->machine,'backgroundColor' => 'rgb(255, 99, 132, .4)','borderColor' => 'rgb(255, 99, 132)','data' => [0,0,0,0,0,0,0,0,0,0 ],'tension' => 0.4,],],]);
                        $chart->setOptions(['maintainAspectRatio' => false,]);
                }elseif ($this->datavalue <= 100 and $this->datavalue > 050) {  // 1    1 minute
                        $chartlabels = ['6H', '7H', '8H', '9H', '10H', '11H', '12H', '13H', '14H', '15H', '16H', '17H', '18H'];
                        $chart = $this->chartBuilder->createChart(Chart::TYPE_BAR);
                        $chart->setData(['labels' => $chartlabels,'datasets' => [['label' => $this->machine,'backgroundColor' => 'rgb(255, 99, 132, .4)','borderColor' => 'rgb(255, 99, 132)','data' => [0,0,0,0,0,0,0,0,0,0 ],'tension' => 0.4,],],]);
                        $chart->setOptions(['maintainAspectRatio' => false,]);
                }elseif ($this->datavalue <= 050 and $this->datavalue >= 000) {  // 0    15 sec
                        $chartlabels = ['6H', '7H', '8H', '9H', '10H', '11H', '12H', '13H', '14H', '15H', '16H', '17H', '18H'];
                        $chart = $this->chartBuilder->createChart(Chart::TYPE_BAR);
                        $chart->setData(['labels' => $chartlabels,'datasets' => [['label' => $this->machine,'backgroundColor' => 'rgb(255, 99, 132, .4)','borderColor' => 'rgb(255, 99, 132)','data' => [0,0,0,0,0,0,0,0,0,0 ],'tension' => 0.4,],],]);
                        $chart->setOptions(['maintainAspectRatio' => false,]);                                        
                }else {
                        $chartlabels = ['6H', '7H', '8H', '9H', '10H', '11H', '12H', '13H', '14H', '15H', '16H', '17H', '18H'];
                        $chart = $this->chartBuilder->createChart(Chart::TYPE_BAR);
                        $chart->setData(['labels' => $chartlabels,'datasets' => [['label' => $this->machine,'backgroundColor' => 'rgb(255, 99, 132, .4)','borderColor' => 'rgb(255, 99, 132)','data' => [0,0,0,0,0,0,0,0,0,0 ],'tension' => 0.4,],],]);
                        $chart->setOptions(['maintainAspectRatio' => false,]);
                }



                return $chart;
        }


            public function camembertdesigne()
            {
                $camembert = $this->chartBuilder->createChart(Chart::TYPE_PIE);
                $camembert->setData([
                    'labels' => ['Vitesse reduite', 'En Arret', 'En travail'],
                
                    'datasets' => [
                        [
                            // 'label' => 'Cookies  🍪',
                            'backgroundColor' => ['rgb(255, 99, 132, .4)','rgb(255, 88, 132)','rgba(45, 220, 126, .4)'],
                            'borderColor' => ['rgb(255, 99, 132)','rgb(255, 99, 132, .4)','rgba(45, 220, 126)'],
                            'data' => [60, 10, 30],
                            'tension' => 0.4,
                        ],
                    ],
                ]);
                $camembert->setOptions([
                    'maintainAspectRatio' => false,
                ]);  

                return $camembert;
            }
    
    #[LiveAction]
    public function getDataStats(): int
    {
        $this->shartdesigne();

        dump($this->datavalue);
        // dump($this->datefin);
        // dump($this->machine);
        // dump($this->selectdate);
        return $this->datavalue;
        // return rand(0, $this->max);
    }
}
