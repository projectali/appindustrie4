<?php

namespace App\Repository;

use App\Entity\TableJournale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TableJournale>
 *
 * @method TableJournale|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableJournale|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableJournale[]    findAll()
 * @method TableJournale[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableJournaleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableJournale::class);
    }

public function getDays(\DateTime $firstDateTime, \DateTime $lastDateTime)
{
    $qb = $this->getEntityManager()->createQueryBuilder()
        ->select('c')
        ->from('ProjectBundle:Calendar', 'c')
        ->where('c.date BETWEEN :firstDate AND :lastDate')
        ->setParameter('firstDate', $firstDateTime)
        ->setParameter('lastDate', $lastDateTime)
    ;

    $result = $qb->getQuery()->getResult();

    return $result;
}

public function getReport($initialDate, $finalDate, $MachineName)
{
    // if ($initialDate <> $finalDate) {
        $startdate = date('Y-m-d', strtotime($initialDate. ' - 1 days')) ; 
        $enddate = date('Y-m-d', strtotime($finalDate. ' + 1 days'));
    $qb = $this->createQueryBuilder('s')
                // ->where('(s.createdAt >= :initialDate) OR  (s.createdAt <= :finalDate)')
                ->where('s.createdAt BETWEEN :initialDate AND :finalDate ')
                ->andWhere('s.machineName = :machinename') 
                ->setParameter('machinename', $MachineName)
                ->setParameter('initialDate', $initialDate . '%')
                ->setParameter('finalDate', $enddate . '%');
    // }
    return $qb->getQuery()->getResult();
}
   /**
    * @return TableJournale[] Returns an array of TableJournale objects
    */
public function getmachinepardate($dateselect, $machine): array
{
    // dump($machine);
    $qb = $this->createQueryBuilder('a')
               ->where('a.createdAt LIKE :datecourant')
               ->andWhere('a.machineName = :machinename')
               ->setParameter('datecourant', $dateselect . '%')
               ->setParameter('machinename', $machine)
            //    ->orderBy('a.createdAt', 'DESC');
            ;

    $result = $qb->getQuery()->getResult();

    return $result;;
}
   /**
    * @return TableJournale[] Returns an array of TableJournale objects
    */
   public function findByExampleField($value): array
   {
       return $this->createQueryBuilder('t')
           ->andWhere('t.exampleField = :val')
           ->setParameter('val', $value)
           ->orderBy('t.id', 'ASC')
           ->setMaxResults(10)
           ->getQuery()
           ->getResult()
       ;
   }

//    public function findOneBySomeField($value): ?TableJournale
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
public function getProducts(string $query): array
{
    if (empty($query)) {
        return [];
    }
    return $this->createQueryBuilder('b')
        ->andWhere('b.machineName LIKE :query')
        ->setParameter('query', '%'.$query.'%')
        ->orderBy('b.id', 'ASC')
        ->getQuery()
        ->getResult()
        ;
}
}
