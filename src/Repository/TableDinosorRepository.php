<?php

namespace App\Repository;

use App\Entity\TableDinosor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TableDinosor>
 *
 * @method TableDinosor|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableDinosor|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableDinosor[]    findAll()
 * @method TableDinosor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableDinosorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableDinosor::class);
    }

//    /**
//     * @return TableDinosor[] Returns an array of TableDinosor objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TableDinosor
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
