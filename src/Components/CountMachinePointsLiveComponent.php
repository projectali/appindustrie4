<?php
namespace App\Components;

use App\Repository\TableJournaleRepository;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent('CountMachinePoint')]
class CountMachinePointsLiveComponent
{
    use DefaultActionTrait;

    public function __construct(private TableJournaleRepository $journalRepository)
    {
    }

    public function getcountProducts(): int
    {
        return $this->journalRepository->count([]);
    }
}