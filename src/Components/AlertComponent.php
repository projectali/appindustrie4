<?php
namespace App\Twig\Components;

use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent('Alert')]
class AlertComponent
{
    public string $type;
    public string $message;
}