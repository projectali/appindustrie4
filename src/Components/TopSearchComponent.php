<?php
namespace App\Twig\Components;

use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent('TopSearch')]
class TopSearchComponent
{
    public $selectdates = array('A', 'B', 'C', 'D');
    // public string $selectdatestringsSearch;
    public $machineNames = array('A', 'B', 'C', 'D');
}