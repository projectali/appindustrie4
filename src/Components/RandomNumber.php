<?php
// src/Components/RandomNumber.php
namespace App\Components;

// use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;

// #[AsTwigComponent('RandomNumber')]
#[AsLiveComponent('RandomNumber')]
class RandomNumber
{
    use DefaultActionTrait;

    #[LiveProp]
    public int $max ;
    #[LiveProp(writable: true)]
    public int $fromYear = 200;
    #[LiveAction]
    public function getRandomNumberfunc(): int
    {
        return $this->fromYear;
        // return rand(0, $this->max);
    }
}