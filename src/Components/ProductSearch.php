<?php
namespace App\Components;

use App\Repository\TableJournaleRepository;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent('ProductSearch')]
class ProductSearch
{
    use DefaultActionTrait;

    #[LiveProp(writable: true)]
    public string $query = '';
    public function __construct(private TableJournaleRepository $journalRepository)
    {
    }

    public function getProducts(): array
    {
        // example method that returns an array of Products
        return $this->journalRepository->getProducts($this->query);
    }
}