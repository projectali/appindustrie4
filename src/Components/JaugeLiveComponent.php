<?php
namespace App\Components;

use App\Entity\TableDinosor;
use App\Service\DinoStatsService;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;
use Symfony\UX\TwigComponent\Attribute\ExposeInTemplate;
use App\Entity\TableJournale;
use App\Repository\TableDinosorRepository;

#[AsLiveComponent('jaugechart')]
class JaugeLiveComponent
{
    
    use DefaultActionTrait;

    #[LiveProp(writable: true)]
    public array $currentTypes = ['all', 'large theropod', 'small theropod'];

    #[LiveProp(writable: true)]
    public int $fromYear = -200;

    #[LiveProp(writable: true)]
    public int $toYear = -65;

    public function __construct(
        // private TableDinosor $dinoStatsService,
        private ChartBuilderInterface $chartBuilder,
        private TableDinosorRepository $tableDinosorRepository,
    ) {
    }
    // dd($toYear);
      #[ExposeInTemplate]
    public function getChart(): Chart
    {
        $chart = $this->chartBuilder->createChart(Chart::TYPE_LINE);
            // $chart->setData($this->dinoStatsService->getDiet(
            //     // $this->fromYear,
            //     // $this->toYear,
            //     // $this->currentTypes
            // ));
            $chart->setData([
            
            // 'labels' => ['6H', '7H', '8H', '9H', '10H', '11H', '12H', '13H', '14H', '15H', '16H', '17H', '18H'],
            'labels' => 'dino',
            // $retVal = ($data[1]->getPointnumber()) ? $data[1]->getPointnumber() : 10 ;
            'datasets' => [
                [
                    'label' => 'dinolabel',
                    'backgroundColor' => 'rgb(255, 99, 132, .4)',
                    'borderColor' => 'rgb(255, 99, 132)',
                    'data' => [
                        (10 ),
                        (0 ),
                        (50 ),
                        (0 ),
                        (0 ),
                        (0 ),
                        (0 ),
                        (20 ),
                        (0 ),
                        (0 ),
                        (0 ),
                        (0 ),
                        (0 ),
                                ],
                    'tension' => 0.4,
                ],
            ],
        ]);
        $chart->setOptions([
            // set title plugin
            'plugins' => [
                'title' => [
                    'display' => true,
                    // 'text' => sprintf(
                    //     'Dinos species count from %dmya to %dmya',
                    //     // abs($this->fromYear),
                    //     // abs($this->toYear)
                    // ),
                ],
                'legend' => [
                    'labels' => [
                        'boxHeight' => 20,
                        'boxWidth' => 50,
                        'padding' => 20,
                        'font' => [
                            'size' => 14,
                        ],
                    ],
                ],
            ],
            'elements' => [
                'line' => [
                    'borderWidth' => 5,
                    'tension' => 0.25,
                    'borderCapStyle' => 'round',
                    'borderJoinStyle' => 'round',
                ],
            ],
            'maintainAspectRatio' => false,
        ]);

        return $chart;
    }  
    
    #[ExposeInTemplate]
    public function getRandomNumber(): int
    {
        
        //  return $this->tableDinosorRepository->findAll();
        // return $this->fromYear;
        // dump($this->query);
        return rand(0, $this->fromYear);
        // return $this->query;
    }
}