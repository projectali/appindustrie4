<?php

namespace App\Controller;

use App\Entity\TableJournale;
use App\Repository\TableJournaleRepository;
use Symfony\UX\Chartjs\Model\Chart;
use Doctrine\ORM\EntityManagerInterface;
// use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\VarDumper\Cloner\Data;

// use App\Service\UxPackageRepository;


class StatisticController extends AbstractController
{
    #[Route('/statistic', name: 'app_statistic')]
    public function index( ChartBuilderInterface $chartBuilder, TableJournaleRepository $repo ): Response
    {
        // $id = 1;

    
        // $repository = $this->getDoctrine()->getRepository(TableJournale::class);
        // $repository = $entityManager->getRepository(TableJournale::class);
        $data = $repo->findOneById(1)->getPointnumber();
        $datas = $repo->findAll();
        // $data = $repo->findBy(0);
        // $package = $packageRepository->find('chartjs');
        // $janv = $data-> .['pointnumber'];
        // var_dump($data['0']["pointnumber"]);
        // var_dump($data);

        // nombre de points par heures
        $chart = $chartBuilder->createChart(Chart::TYPE_LINE);
        $chart->setData([
            
            'labels' => ['6H', '7H', '8H', '9H', '10H', '11H', '12H', '13H', '14H', '15H', '16H', '17H', '18H'],
            'datasets' => [
                [
                    'label' => 'Machine B9  ',
                    'backgroundColor' => 'rgb(255, 99, 132, .4)',
                    'borderColor' => 'rgb(255, 99, 132)',
                    'data' => [$data, 10, 5, 18, 20, 30, 45, 100, 120, 0, 400],
                    'tension' => 0.4,
                ],
                [
                    'label' => 'Machine B15',
                    'backgroundColor' => 'rgba(45, 220, 126, .4)',
                    'borderColor' => 'rgba(45, 220, 126)',
                    'data' => [10, 15, 4, 3, 25, 41, 25],
                    'tension' => 0.4,
                ],
            ],
        ]);
        $chart->setOptions([
            'maintainAspectRatio' => false,
        ]);
        $camembert = $chartBuilder->createChart(Chart::TYPE_PIE);
        $camembert->setData([
            'labels' => ['B9', 'B15', 'B10'],
            
            'datasets' => [
                [
                    // 'label' => 'Cookies  🍪',
                    'backgroundColor' => ['rgb(255, 99, 132, .4)','rgb(255, 88, 132)','rgba(45, 220, 126, .4)'],
                    'borderColor' => ['rgb(255, 99, 132)','rgb(255, 99, 132, .4)','rgba(45, 220, 126)'],
                    'data' => [$data, 10, 5, 18, 20, 30, 45],
                    'tension' => 0.4,
                ],
                // [
                //     'label' => 'Km walked 🏃‍♀️',
                //     'backgroundColor' => 'rgba(45, 220, 126, .4)',
                //     'borderColor' => 'rgba(45, 220, 126)',
                //     'data' => [10, 15, 4, 3, 25, 41, 25],
                //     'tension' => 0.4,
                // ],
            ],
        ]);
        $camembert->setOptions([
            'maintainAspectRatio' => false,
        ]);

        return $this->render('statistic/statistic.html.twig', [
            'camembert' => $camembert,
            'chart' => $chart,
            'datas' => $datas,
        ]);
    }
}
